use std::collections::HashSet;

// You should implement the following function:
fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 {
    let mut num1 = multiple1;
    let mut num2 = multiple2;
    let mut multiples = HashSet::new();

    // Add multiples of multiple1 to hash set
    while num1 < number {
        multiples.insert(num1);
        num1 += multiple1;
    }

    // Add multiples of multiple2 to hash set
    while num2 < number {
        multiples.insert(num2);
        num2 += multiple2;
    }

    // Iterate over hash set
    let mut sum = 0;
    for num in multiples {
        sum += num;
    }

    return sum;
}

fn main() {
    println!("Final answer: {}", sum_of_multiples(1000, 3, 5));
}
