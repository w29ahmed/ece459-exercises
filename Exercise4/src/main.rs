use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let num_messages = 5;

    let (tx, rx) = mpsc::channel();
    // let tx1 = mpsc::Sender::clone(&tx);

    // Sending thread
    let thread1 = thread::spawn(move || {
        for i in 0..num_messages {
            println!("Sending message {}", i);
            tx.send(i).unwrap();
            thread::sleep(Duration::from_secs(1));
        }
    });

    // Receiving thread
    let thread2 = thread::spawn(move || {
        for _i in 0..num_messages {
            let rcv = rx.recv().unwrap();
            println!("Received message {}", rcv);
        }
    });

    thread1.join().unwrap();
    thread2.join().unwrap();
    println!("All threads finished.");
}
