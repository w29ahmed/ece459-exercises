use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut threads = vec![];

    for i in 0..N {
        threads.push(thread::spawn(move || {
            println!("Hello from thread {}", i);
        }));
    }

    for thread in threads {
        let _ = thread.join();
    }
    println!("All threads finished.");
}
