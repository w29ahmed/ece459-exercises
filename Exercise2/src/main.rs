// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n == 0 {
        return 0;
    }
    if n == 1 {
        return 1;
    }

    let mut first = 0;
    let mut second = 1;
    for _i in 2..=n {
        let third = first + second;
        first = second;
        second = third;
    }

    return second;
}

fn main() {
    println!("Final answer: {}", fibonacci_number(10));
}
